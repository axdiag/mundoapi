Instrucciones de instalación

1- Clonar el repositorio en ambiente local
git clone https://gitlab.com/axdiag/mundoapi.git

2- 
    2.1 Crear base de datos ingresando a la terminal mysql con el siguiente comando:
    CREATE DATABASE bodegaMundo;
    2.2 Selecionar base de datos con el siguiente comando:
    USE bodegaMundo;

3- En caso de ser manejar credenciales para mysql se deben ingresar en el archivo .env ubicado en la raiz
   del proyecto asignando los valores correspondientes a las variables DB_USERNAME y DB_PASSWORD

   Estando en la carpeta del proyecto, ejecutar los siguiente comandos

   php artisan migrate --seed (creación de tablas y llenado de base de datos)
   php artisan serve

4- Lanzar aplicación de react "mundoweb"







   