<?php

use App\Http\Controllers\API\DispositivoController;
use App\Http\Controllers\API\ModeloController;
use App\Http\Controllers\API\MarcaController;
use App\Http\Controllers\API\BodegaController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;



Route::get('dispositivos', [DispositivoController::class, 'index']);
Route::get('bodegas', [DispositivoController::class, 'send']);
Route::get('modelos/{id}', [DispositivoController::class, 'find']);

Route::get('filtromodelo/{id}', [ModeloController::class, 'filtro']);
Route::get('filtromarca/{id}', [MarcaController::class, 'filtro']);
Route::get('filtrobodega/{id}', [BodegaController::class, 'filtro']);

Route::post('ingresar-dispositivo', [DispositivoController::class, 'store']);

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
