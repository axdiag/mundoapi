<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Dispositivo;


class MarcaController extends Controller
{
    public function filtro($id){

        if ($id == 0) {
            $dispositivos = Dispositivo::leftJoin('bodegas', 'bodegas.id', '=', 'dispositivos.bodega_id')
                                        ->join('modelos', 'modelos.id', '=', 'dispositivos.modelo_id')
                                        ->join('marcas', 'marcas.id', '=', 'modelos.marca_id')
                                        ->select('dispositivos.*', 'bodegas.bodega', 'modelos.modelo', 'marcas.marca')
                                        ->orderBy('dispositivos.id')
                                        ->get();


        }
        else{
            $dispositivos = Dispositivo::leftJoin('bodegas', 'bodegas.id', '=', 'dispositivos.bodega_id')
                                        ->join('modelos', 'modelos.id', '=', 'dispositivos.modelo_id')
                                        ->join('marcas', 'marcas.id', '=', 'modelos.marca_id')
                                        ->where('marcas.id', $id)
                                        ->select('dispositivos.*', 'bodegas.bodega', 'modelos.modelo', 'marcas.marca')
                                        ->orderBy('dispositivos.id')
                                        ->get();
        }
        return response()->json(
            [
                'status' => 200,
                'dispositivos' => $dispositivos
            ]
        );
    }
}
