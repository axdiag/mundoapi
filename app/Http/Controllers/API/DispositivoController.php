<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Bodega;
use App\Models\Dispositivo;
use App\Models\Marca;
use App\Models\Modelo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DispositivoController extends Controller
{

    public function index()
    {
        
        $dispositivos = Dispositivo::leftJoin('bodegas', 'bodegas.id', '=', 'dispositivos.bodega_id')
                                    ->join('modelos', 'modelos.id', '=', 'dispositivos.modelo_id')
                                    ->join('marcas', 'marcas.id', '=', 'modelos.marca_id')
                                    ->select('dispositivos.*', 'bodegas.bodega', 'modelos.modelo', 'marcas.marca')
                                    ->orderBy('dispositivos.id')
                                    ->get();

        $bodegas = Bodega::select('id', 'bodega')->get();
        $marcas = Marca::select('id', 'marca')->get();
        $modelos = Modelo::select('id', 'modelo')->get();
      
        return response()->json([
            'status' => 200,
            'dispositivos' => $dispositivos,
            'bodegas' => $bodegas,
            'marcas' => $marcas,
            'modelos' => $modelos
        ]);
    }

    public function send()
    {
        $bodegas = Bodega::select('id','bodega')->get();
        $marcas = Marca::select('id', 'marca')->get();
        $modelos = Modelo::select('id', 'modelo')->get();

        return response()->json(
            [
                'status' => 200,
                'bodegas' => $bodegas,
                'marcas' => $marcas,
                'modelos' => $modelos
            ]
            );

    }

    public function find($id){

       $modelos = Modelo::select('id', 'modelo')->where('marca_id', $id)->get();

        

        return response()->json(
            [
                'modelos' => $modelos
            ]
            );
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'dispositivo'=>'required',
            'bodega'=>'required',
            'marca'=>'required',
            'modelo'=>'required'
        ]);

        if ($validator->fails()) {
            return response()->json(
                [
                    'validation_ERR' => $validator->getMessageBag()
                ]
            );
        }
        else {
            $dispositivo = new Dispositivo();
    
            $dispositivo->bodega_id = (int)$request->input('bodega');
            $dispositivo->dispositivo = $request->input('dispositivo');
            $dispositivo->modelo_id = (int)$request->input('modelo');
            $dispositivo->save();
        
            return response()->json(
                [
                    'status' => 200,
                    'message' => 'Dispositivo ingresado' 
                ]
            );
        }
    

        $dispositivo = new Dispositivo();
    
        $dispositivo->bodega_id = (int)$request->input('bodega');
        $dispositivo->dispositivo = $request->input('dispositivo');
        $dispositivo->modelo_id = (int)$request->input('modelo');
        $dispositivo->save();
    
        return response()->json(
            [
                'status' => 200,
                'message' => 'Dispositivo ingresado' 
            ]
        );
    }
}
