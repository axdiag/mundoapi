<?php

namespace Database\Seeders;

use App\Models\Bodega;
use App\Models\Dispositivo;
use App\Models\DispositivoModeloR;
use Illuminate\Database\Seeder;
use App\Models\Marca;
use App\Models\Modelo;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Marca::factory(3)->create();
        Bodega::factory(3)->create();
        Modelo::factory(10)->create();
        Dispositivo::factory(10)->create();
    }
}
