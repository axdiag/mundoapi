<?php

namespace Database\Factories;

use App\Models\Bodega;
use App\Models\Modelo;
use Illuminate\Database\Eloquent\Factories\Factory;

class DispositivoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'dispositivo'=>$this->faker->randomElement(['Switch', 'Router']),
            'bodega_id'=>$this->faker->randomElement(Bodega::pluck('id')),
            'modelo_id'=>$this->faker->randomElement(Modelo::pluck('id'))
        ];
    }
}
