<?php

namespace Database\Factories;

use App\Models\Marca;
use Illuminate\Database\Eloquent\Factories\Factory;

class ModeloFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'modelo'=>$this->faker->unique()->randomElement(['xs-50','xs-40','xs-30','xs-20','xs-10','s-10','s-20', 's-30', 'm-50', 'l-50']),
            'marca_id'=>$this->faker->randomElement(Marca::pluck('id'))
        ];
    }
}
